package com.vti.form;

import com.vti.entity.Department;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountCreateForm {

    private String username;
    private Integer departmentId;
}
